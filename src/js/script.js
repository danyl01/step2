const menuBtn = document.querySelector(".main-header__menu-button");
const navigationMenu = document.querySelector(".main-header__navigation");
menuBtn.addEventListener("click", (event) => {
    menuBtn.classList.toggle("active");
    navigationMenu.classList.toggle("active");
});
document.addEventListener("click", (event) => {
    let target = event.target;
    console.log(target);
    let isMenu = target === navigationMenu || navigationMenu.contains(target);
    let isBtn = target === menuBtn;
    let menuIsActive = navigationMenu.classList.contains("active");
    if (!isMenu && !isBtn && menuIsActive) {
        navigationMenu.classList.toggle("active");
        menuBtn.classList.toggle("active");
    }
})

